# Robot Framework with Browser library

This project creates an Docker image with Robot Framework and Browser library. The base image used to create this images 
is based [marketsquare/robotframework-browser](https://hub.docker.com/r/marketsquare/robotframework-browser/tags) on see
[Docker Images for Robot Framework](https://docs.robotframework.org/docs/using_rf_in_ci_systems/docker).



- [Build locally](#build-locally)
- [Pull images](#pull-images)
- [Verify](#verify)
  - [Robot version](#robot-version)
  - [Python version](#python-version)
  - [Run simple test](#run-simple-test)
- [Troubleshooting](#troubleshooting)
- [Known issues](#known-issues)

## Build locally

```bash
docker build -t robot:latest .
docker run --name robot --rm --it robot:latest bash
```

## Pull images


```bash
docker pull registry.gitlab.com/c2platform/docker/robot:latest
docker run --name robot --rm --it registry.gitlab.com/c2platform/docker/robot:latest bash
```

## Verify

### Robot version

Verify Robot version

```bash
onknows@io1:~/git/gitlab/c2/docker/robot$ docker run --rm -it -v $(pwd)/test/:/test --ipc=host --user pwuser marketsquare/robotframework-browser:latest bash
pwuser@87d134a5bd8f:/$ robot --version
Robot Framework 6.0.2 (Python 3.8.10 on linux)
pwuser@87d134a5bd8f:/$

```

### Python version

```
onknows@io1:~/git/gitlab/c2/docker/robot$ docker run --rm -it -v $(pwd)/test/:/test --ipc=host --user pwuser marketsquare/robotframework-browser:latest bash
pwuser@69855bc35e3f:/$ python3 --version
Python 3.8.10

```

### Run simple test

```bash
onknows@io1:~/git/gitlab/c2/docker/robot$ docker run --rm -v $(pwd)/test/:/test --ipc=host --user pwuser marketsquare/robotframework-browser:latest bash -c "robot --outputdir /test/output /test"
==============================================================================
Test
==============================================================================
Test.Firefox
==============================================================================
Example Test                                                          | PASS |
------------------------------------------------------------------------------
Test.Firefox                                                          | PASS |
1 test, 1 passed, 0 failed
==============================================================================
Test                                                                  | PASS |
1 test, 1 passed, 0 failed
==============================================================================
Output:  /test/output/output.xml
Log:     /test/output/log.html
Report:  /test/output/report.html
```

## Troubleshooting

## Known issues




